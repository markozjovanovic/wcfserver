﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace ServiceStarter
{
    public abstract class BaseWindowsService<T>
    {
        protected ServiceHost host = null;

        public void Start()
        {
            Console.WriteLine("Starting service");

            //Logging configuration
            ConfigureLogger();

            //Exception handler configuration
            ConfigureExceptionHandler();

            //Dependency injection configuration
            IUnityContainer container = new UnityContainer();
            RegisterTypes(container);

            //Starting service host
            host = new UnityServiceHost(container, typeof(T));
            host.Open();
        }

        public void Stop()
        {
            Console.WriteLine("Stopping service");
            host.Close();
        }

        private void ConfigureLogger()
        {
            //DatabaseProviderFactory factory = new DatabaseProviderFactory(new SystemConfigurationSource(false).GetSection);
            //DatabaseFactory.SetDatabaseProviderFactory(factory, false);

            //var logWritter = new LogWriterFactory().Create();
            //Logger.SetLogWriter(logWritter);
        }

        private void ConfigureExceptionHandler()
        {
            ExceptionPolicyFactory policyFactory = new ExceptionPolicyFactory();
            var exManager = policyFactory.CreateManager();
            ExceptionPolicy.SetExceptionManager(exManager);
        }

        protected abstract void RegisterTypes(IUnityContainer container);
    }
}
